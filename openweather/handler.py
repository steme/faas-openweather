import openweathermapy.core as owm
import json
import os

def handle(req):
    appid = os.environ['APPID']
    settings = {"APPID": appid ,"units": "metric", "lang": "DE"}
    data = owm.get_current(req, **settings)
    print(json.dumps(data))